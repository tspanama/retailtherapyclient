package com.rbs.retailtherapy.client;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class App {

	private final static Logger logger = Logger.getLogger(App.class.getName());
	private static ConsoleHandler consoleHandler;

	public static void main(String[] args) {
		String username = "Pulsy";
		String password = "testing";
		String baseUrl = "http://localhost:8081/RetailTherapy/jsonServices";
		try {
			logger.setLevel(Level.FINE);
			consoleHandler = new ConsoleHandler();
			consoleHandler.setFormatter(new SimpleFormatter());
			consoleHandler.setLevel(Level.FINE);
			logger.addHandler(consoleHandler);
			MyAwesomeSolution client = new MyAwesomeSolution(baseUrl);
			client.start(username, password);
		} catch (Exception e) {
			logger.severe(e.getStackTrace()[1].getClass().getSimpleName() + ": " + e.getMessage());
		}
	}
}
